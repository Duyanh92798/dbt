# Các lệnh thường dùng
```shell
dbt compile --profiles-dir profile
dbt docs generate --profiles-dir profile
dbt docs serve --profiles-dir profile
dbt run --profiles-dir profile --fail-fast --vars '{"etl_date": "2023-05-01"}'
dbt --log-format=json run --profiles-dir profile --fail-fast --vars '{"etl_date": "2023-05-01"}' --select stg_shop_customers
dbt run --profiles-dir profile --fail-fast --vars '{"etl_date": "2023-05-01"}' --select stg_shop_customers --full-refresh --log-format=json
```

### Chạy tiếp từ lần chạy lỗi
Tham khảo: https://docs.getdbt.com/docs/deploy/project-state
```shell
dbt run --profiles-dir profile --vars '{"etl_date": "2023-05-01"}' --full-refresh --select result:<status>+ --state ./<dbt-artifact-path>
dbt run --profiles-dir profile --vars '{"etl_date": "2023-05-01"}' --full-refresh --select result:error+ --state target
```

- `--state`: thư mục artifact, chứa file `run_results.json`. Mỗi lần chạy thì file `run_results.json` sẽ bị ghi đè nên khi có lỗi thì backup cả thư mục artifact để có thể chạy tiếp
- `--select result:<status>+`: chạy tiếp từ model bị lỗi trở đi. Các option status khác biệt giữa các lệnh chạy, được mô tả trong bảng sau:

| Tables        |model|seed |snapshot|test |
| ------------- |:---:|:---:|:------:|:---:|
|result:error	|✅|✅|✅|✅|
|result:success	|✅|✅|✅|	|
|result:skipped	|✅|	|✅|✅|
|result:fail	|	|	|✅|	|
|result:warn	|	|	|✅|	|
|result:pass	|	|	|✅|	|


# Cài đặt formater

## Thêm thư viện format vào Python
```sql
py -m pip install sqlfluff
py -m pip install sqlfluff-templater-dbt
```

Cài thêm thư viện `sqlfluff-templater-dbt` để parse lệnh sql chuẩn hơn, nhưng phải truy cập DB (với model cần truy cập DB khi compile) nên sẽ chậm hơn. Tham khảo: https://docs.sqlfluff.com/en/stable/configuration.html#dbt-project-configuration

## Cài đặt VS Code
1. Cài đặt extension `vscode-sqlfluff` để tự động chạy lệnh phát hiện lỗi khi viết model
2. Cấu hình `setting.json`
    - Ấn `Ctrl` + `Shift` + `P`
    - Nhập `>settings.json`
    - Chọn `Preferences: Open User Settings (JSON)`
    - Thêm key cho file
    ```json
    {
        "sqlfluff.linter.run": "onSave",
        "sqlfluff.experimental.format.executeInTerminal": true,
        "editor.formatOnSave": false
    }
    ```

## Cấu hình trong project

### Cấu hình sqlfluff
Là file định nghĩa rule và cách sqlfluff hoạt động

Thêm file `.sqlfluff` vào thư mục root của project. File này để định nghĩa cách parse SQL. Để điều chỉnh rule kiểm tra format, tham khảo https://docs.sqlfluff.com/en/stable/rules.html

Các file sau đều có thể dùng để cấu hình sqlfluff: `setup.cfg`, `tox.ini`, `pep8.ini`, `.sqlfluff`

Ví dụ nội dung file:
```ini
[sqlfluff]
templater = jinja
dialect = postgres

[sqlfluff:layout:type:comma]
line_position = leading

[sqlfluff:layout.cte_newline]
cte_newline = False

[sqlfluff:capitalisation.keywords]
capitalisation_policy = lower

[sqlfluff:capitalisation.functions]
capitalisation_policy = lower
```

### Cấu hình sqlfluffignore
Là file mô tả file và thư mục ngoại lệ để không parse SQL. Tương tự `.gitignore`

Thêm file `.sqlfluffignore` vào thư mục root của project

Ví dụ nội dung file:

```
target/
dbt_packages/
macros/
```