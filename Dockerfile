FROM python:3.11.4-bullseye

USER root

RUN ls

RUN mkdir /dbt

COPY ./* /dbt

WORKDIR /dbt

RUN ls

RUN pip install dbt-core dbt-postgres

CMD ["dbt","run"]
