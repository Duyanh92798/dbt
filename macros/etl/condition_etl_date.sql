{% macro condition_etl_date(column_ppn_dt, etl_type) -%}
{%- if etl_type == 'day' -%}
({{column_ppn_dt}} >= to_date('{{ var("etl_date") }}', 'yyyy-mm-dd') and {{column_ppn_dt}} < to_date('{{ var("etl_date") }}', 'yyyy-mm-dd') + 1)
{%- elif etl_type == 'month' -%}
({{column_ppn_dt}} >= to_date({{ var('etl_date') }}, 'yyyy-mm-dd') and {{column_ppn_dt}} < to_date({{ var('etl_date') }}, 'yyyy-mm-dd') + 1)
{%- endif -%}
{%- endmacro %}