{% docs condition_etl_date %}
Điều kiện lọc dữ liệu theo tham số ngày etl. Tham số cố định tên là `etl_date`.

Tham số:
1. Tên của cột thời gian trong bảng dữ liệu cần lọc theo `etl_date`.
2. Thời lượng cần lấy từ bảng nguồn. Có giá trị:
    - `day`: lấy dữ liệu trong ngày etl
    - `day-1`: lấy dữ liệu trong ngày trước ngày etl
    - `month`: lấy dữ liệu trong tháng etl
    - `month-1`: lấy dữ liệu trong tháng trước tháng etl
    - `year`: lấy dữ liệu trong năm etl
    - `year-1`: lấy dữ liệu trong năm trước năm etl
{% enddocs %}