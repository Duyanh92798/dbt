{%- macro to_date_etl_date() -%}
to_date('{{ var("etl_date") }}', 'yyyy-mm-dd')
{%- endmacro -%}