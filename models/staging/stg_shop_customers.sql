{{
  config(
    materialized = 'incremental',
    pre_hook=["delete from {{ this }} where {{ condition_etl_date('eff_dt', 'day') }} or {{ condition_etl_date('updated_date', 'day') }}"]
    )
}}

with src_customer as (
    select * from {{ source('src_shop', 'customers') }}
), final as (
    select id as customer_id
        , name
        , email
        , dob
        , created_date as eff_dt
        , updated_date
        , {{ to_date_etl_date() }} as etl_date
    from src_customer
    {% if is_incremental() %}
    where {{ condition_etl_date('created_date', 'day') }}
        or {{ condition_etl_date('updated_date', 'day') }}
    {% endif %}
)
select *
from final
