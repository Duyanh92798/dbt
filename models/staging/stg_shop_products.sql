with src_products as (
    select * from {{ source('src_shop', 'products') }}
)
, final as (
    select id as product_id
        , name
        , price
    from src_products
)
select *
from final
