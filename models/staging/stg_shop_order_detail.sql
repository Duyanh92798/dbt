{{
  config(
    materialized = 'incremental'
    , pre_hook=["delete from {{ this }} where order_id in (select order_id from {{ ref('stg_shop_orders_trg') }} where dml in ('D', 'U') and {{ condition_etl_date('ppn_dt', 'day') }})"]
    )
}}

with src_order_detail as (
  select * from {{ source('src_shop', 'order_detail') }}
), stg_shop_orders_trg as (
  select * from {{ ref('stg_shop_orders_trg') }}
), final as (
  select id as order_detail_id
    , id_order as order_id
    , product_id
    , quantity
  from src_order_detail
  {% if is_incremental() %}
  where id_order in (
    select order_id
    from stg_shop_orders_trg
    where dml in ('I', 'U')
      and {{ condition_etl_date('ppn_dt', 'day') }}
  )
  {% endif %}
)
select *
from final