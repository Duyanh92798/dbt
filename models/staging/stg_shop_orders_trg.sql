{# Bảng chứa bản ghi thay đổi của bảng orders thông qua trigger #}
with aa as (
    select * from {{ source('src_shop', 'orders_trg') }}
), final as (
    select id as order_id
	    , status_code
        , delivered_date
        , dml
        , ppn_dt
    from aa
    where {{ condition_etl_date('ppn_dt', 'day') }}
)
select *
from final
