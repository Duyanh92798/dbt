{{
  config(
    materialized = 'incremental'
    , unique_key=['order_id']
    , pre_hook=["delete from {{ this }} where order_id in (select order_id from {{ ref('stg_shop_orders_trg') }} where {{ condition_etl_date('updated_date', 'day') }})"]
    )
}}

with orders as (
    select * from {{ source('src_shop', 'orders') }}
), stg_shop_orders_trg as (
  select * from {{ ref('stg_shop_orders_trg') }}
), final as (
  select aa.order_id
    , bb.order_date
    , aa.status_code
    , aa.delivered_date
    , bb.description
    , bb.id_customer as customer_id
    , aa.ppn_dt as updated_date
    , {{ to_date_etl_date() }} as eff_dt
  from (
    select order_id, status_code, delivered_date, ppn_dt
      , row_number() over (partition by order_id order by ppn_dt desc) as rn
    from stg_shop_orders_trg
    where dml in ('I', 'U')
    {% if is_incremental() %}
      and {{ condition_etl_date('ppn_dt', 'day') }}  
    {% endif %}
  ) aa
  join orders bb on bb.id = aa.order_id
  where rn = 1
)
select *
from final