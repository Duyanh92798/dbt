{{
  config(
    materialized = 'incremental'
    , unique_key=['hk_customer']
    , pre_hook=["delete from {{ this }} where etl_date = {{ to_date_etl_date() }}"]
    )
}}

{{ simple_cte([
    ('stg_shop_customers', 'stg_shop_customers')
]) }}
, final as (
    select md5(customer_id::varchar) as hk_customer
        , customer_id
        , eff_dt
        , etl_date
    from stg_shop_customers
    {% if is_incremental() %}
    where etl_date = {{ to_date_etl_date() }}
    {% endif %}
)
select *
from final