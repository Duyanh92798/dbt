{{
  config(
    materialized = 'incremental'
    , unique_key=['hk_l_order__customer']
    , pre_hook=["delete from {{ this }} where eff_dt = {{ to_date_etl_date() }}"]
    )
}}

{{ simple_cte([
    ('stg_shop_orders', 'stg_shop_orders')
]) }}
, final as (
    select md5(order_id::varchar || customer_id::varchar) as hk_l_order__customer
        , md5(order_id::varchar) as hk_order_id
        , md5(customer_id::varchar) as hk_customer
        , eff_dt
    from stg_shop_orders
    {% if is_incremental() %}
    where eff_dt = {{ to_date_etl_date() }}
    {% endif %}
)
select *
from final