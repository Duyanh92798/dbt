{{
  config(
    materialized = 'incremental'
    , unique_key=['hk_order_id']
    , pre_hook=["delete from {{ this }} where order_id in (select order_id from {{ ref('stg_shop_orders') }} where eff_dt = {{ to_date_etl_date() }})"]
    )
}}

{{ simple_cte([
    ('stg_shop_orders', 'stg_shop_orders')
]) }}
, final as (
    select md5(order_id::varchar) as hk_order_id
        , order_id
        , eff_dt
    from stg_shop_orders
    {% if is_incremental() %}
    where eff_dt = {{ to_date_etl_date() }}
    {% endif %}
)
select *
from final