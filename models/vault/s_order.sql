{{
  config(
    materialized = 'incremental'
    , pre_hook=["delete from {{ this }} where hk_order_id in (select md5(order_id::varchar) from {{ ref('stg_shop_orders_trg') }} where dml in ('D', 'U') and {{ condition_etl_date('ppn_dt', 'day') }})"]
    )
}}

{{ simple_cte([
    ('stg_shop_orders', 'stg_shop_orders'),
    ('stg_shop_orders_trg', 'stg_shop_orders_trg')
]) }}
, final as (
    select md5(order_id::varchar) as hk_order_id
        , eff_dt
        , order_date
        , delivered_date
        , status_code
        , description
    from stg_shop_orders
    {% if is_incremental() %}
    where order_id in (
        select order_id
        from stg_shop_orders_trg
        where dml in ('I', 'U')
            and {{ condition_etl_date('ppn_dt', 'day') }}
    )
    {% endif %}
)
select *
from final